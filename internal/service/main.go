package service

import (
	"command/api"
	"command/config"
	"command/util/server"
	"command/util/server/grpc/health"
	"context"
	"fmt"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Service struct {
	cfg *config.Config
	health.UnimplementedHealthCheckServiceServer
	api.UnimplementedCommandServiceServer
}

func NewService(cfg *config.Config) (*Service, error) {
	return &Service{}, nil
}

// RegisterWithServer implementing service server interface
func (s *Service) RegisterWithServer(server *grpc.Server) {
	health.RegisterHealthCheckServiceServer(server, s)
	api.RegisterCommandServiceServer(server, s)
}

// RegisterWithHandler implementing service server interface
func (s *Service) RegisterWithHandler(ctx context.Context, mux *runtime.ServeMux, conn *grpc.ClientConn) error {
	err := api.RegisterCommandServiceHandler(ctx, mux, conn)
	if err != nil {
		fmt.Printf("Error register servers: %s", err)
	}
	err = health.RegisterHealthCheckServiceHandler(ctx, mux, conn)
	if err != nil {
		fmt.Printf("Error register health servers: %s", err)
	}

	return nil
}

func (s *Service) Ping() error {
	//err := s.store.Ping()
	return nil
}

func (s *Service) Close(ctx context.Context) {

}

// Liveness handle socket is open or not
func (s *Service) Liveness(ctx context.Context, req *health.LivenessRequest) (*health.LivenessResponse, error) {
	err := s.Ping()
	if err != nil {
		return nil, err
	}

	return &health.LivenessResponse{
		Content: "ok",
	}, nil
}

// Readiness handle application is ready or not
// this should take into account, saturation of the pod, instead
func (s *Service) Readiness(ctx context.Context, req *health.ReadinessRequest) (*health.ReadinessResponse, error) {
	if server.IsServerShuttingDown() {
		return nil, status.Error(codes.Unavailable, "service is shutting down")
	}

	return &health.ReadinessResponse{
		Content: "ok",
	}, nil
}
