package service

import (
	"command/api"
	"context"
)

func (s *Service) Example(context.Context, *api.ExampleRequest) (*api.ExampleResponse, error) {
	return &api.ExampleResponse{
		Success: true,
		Message: "success",
		Code:    1,
	}, nil
}
