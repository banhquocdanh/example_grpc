FROM golang:1.16-buster as builder
WORKDIR /app

COPY ./go.mod ./go.sum ./

RUN go mod download
COPY ./ ./
RUN CGO_ENABLED=0  CGO_CFLAGS_ALLOW=-Xpreprocessor go build -installsuffix 'static' -o /app/main /app/main

FROM gcr.io/distroless/static 
ENV TZ="Asia/Ho_Chi_Minh"
USER nonroot:nonroot

COPY --from=builder --chown=nonroot:nonroot /app/config_example.yaml /app/config.yaml
COPY --from=builder --chown=nonroot:nonroot /app/main /app/main

WORKDIR /app
ENTRYPOINT ["/app/main", "server"]
