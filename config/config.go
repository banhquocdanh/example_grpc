package config

import (
	"bytes"
	"command/util"
	"command/util/server"
	"encoding/json"
	"github.com/spf13/viper"
	"log"
	"strings"
)

// Config application
type Config struct {
	ServiceName string `yaml:"service_name" mapstructure:"service_name"`
	Env         string `json:"env" yaml:"env" mapstructure:"env"`
	Log         util.LogCfg `json:"log" yaml:"log" mapstructure:"log"`

	Server ServerConfig `json:"server" yaml:"server" mapstructure:"server"`
}

type ServerConfig struct {
	GRPC server.Listen `json:"grpc" mapstructure:"grpc" yaml:"grpc"`
	HTTP server.Listen `json:"http" mapstructure:"http" yaml:"http"`
}

func DefaultConfig() *Config {
	return &Config{
		ServiceName: "command",
		Env:         "DEV",
		Log: util.LogCfg{
			Development: true,
			Level:       "debug",
			Encoding:    "json",
		},
		Server: ServerConfig{
			GRPC: server.Listen{
				Host: "0.0.0.0",
				Port: 10000,
			},
			HTTP: server.Listen{
				Host: "0.0.0.0",
				Port: 9000,
			},
		},
	}
}


func LoadConfig() (*Config, error) {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("./config/")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "__"))
	viper.AutomaticEnv()
	/**
	|-------------------------------------------------------------------------
	| You should set default config value here
	| 1. Populate the default value in (Source code)
	| 2. Then merge from config (YAML) and OS environment
	|-----------------------------------------------------------------------*/
	c := DefaultConfig()
	if configBuffer, err := json.Marshal(c); err != nil {
		log.Println("Oops! Marshal config is failed. ", err)
		return nil, err
	} else if err := viper.ReadConfig(bytes.NewBuffer(configBuffer)); err != nil {
		log.Println("Oops! Read default config is failed. ", err)
		return nil, err
	}
	if err := viper.MergeInConfig(); err != nil {
		log.Println("Read config file failed.", err)
	}
	// Populate all config again
	err := viper.Unmarshal(c)
	return c, err
}
