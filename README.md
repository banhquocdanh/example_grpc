# Command service

##run
- make install-go-tools
- go mod vendor
- go run ./main server

##structure
  - ./api
    + define api as proto
  - ./internal/service
    + implement handler for api

## Example
- define api
  ```protobuf
    rpc Example (ExampleRequest) returns (ExampleResponse) {
        option (google.api.http) = {
          post: "/api/example",
          body: "*"
        };
  }
  ```

- implement api handler
    ```go
    func (s *Service) Example(context.Context, *api.ExampleRequest) (*api.ExampleResponse, error) {
        return &api.ExampleResponse{
            Success: true,
            Message: "success",
            Code:    1,
        }, nil
    }
    ```
 
- check new device