GOFMT_FILES?=$$(find . -name '*.go' | grep -v vendor)
GOFMT := "goimports"

GO_TOOLS = 	google.golang.org/grpc/cmd/protoc-gen-go-grpc \
			github.com/gogo/protobuf/protoc-gen-gofast \
			google.golang.org/protobuf/cmd/protoc-gen-go \
			github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway \
			github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2 \
			github.com/envoyproxy/protoc-gen-validate \
			github.com/bufbuild/buf/cmd/protoc-gen-buf-breaking \
			github.com/bufbuild/buf/cmd/protoc-gen-buf-lint \
			github.com/pseudomuto/protoc-gen-doc/cmd/protoc-gen-doc \
			github.com/bold-commerce/protoc-gen-struct-transformer \
			github.com/googleapis/api-linter/cmd/api-linter \
			github.com/google/wire/cmd/wire \
			github.com/rakyll/statik \
			github.com/kyleconroy/sqlc \
			github.com/kyleconroy/sqlc/cmd/sqlc \
			github.com/go-swagger/go-swagger/cmd/swagger \
			google.golang.org/grpc/cmd/protoc-gen-go-grpc \
            github.com/envoyproxy/protoc-gen-validate \
            github.com/bufbuild/buf/cmd/buf \


install-go-tools:
	go get $(GO_TOOLS)

fmt: ## Run gofmt for all .go files
	@$(GOFMT) -w $(GOFMT_FILES)

generate: ## Generate proto
	buf generate

test: ## Run go test for whole project
	@go test -v ./...

lint: ## Run linter
	@golangci-lint run ./...

help: ## Display this help screen
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
