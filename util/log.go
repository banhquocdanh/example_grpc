package util

import (
	"context"
	"github.com/go-logr/logr"
	"github.com/go-logr/zapr"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"strings"
)

var l logr.Logger

func init() {
	defaultCfg := DefaultConfig()
	pl, _ := NewLogger(&defaultCfg)
	l = *pl
}

type levelMap map[string]zapcore.Level

func (l levelMap) Get(level string) (zapcore.Level, bool) {
	zapLevel, ok := l[strings.ToUpper(level)]
	return zapLevel, ok
}

var zapLevelMap = levelMap{
	"debug":  zap.DebugLevel,
	"info":   zap.InfoLevel,
	"warn":   zap.WarnLevel,
	"error":  zap.ErrorLevel,
	"dpanic": zap.DPanicLevel,
	"panic":  zap.PanicLevel,
	"fatal":  zap.FatalLevel,
}

type LogCfg struct {
	Development bool   `json:"development" yaml:"development"`
	Level       string `json:"level" yaml:"level"`
	Encoding    string `json:"encoding" yaml:"encoding"`
}

func DefaultConfig() LogCfg {
	return LogCfg{
		Development: false,
		Level:       "error",
		Encoding:    "console",
	}
}

func NewLogger(cfg *LogCfg) (*logr.Logger, error) {
	zapConfig := zap.NewProductionConfig()

	if cfg.Development {
		zapConfig.Level = zap.NewAtomicLevelAt(zap.DebugLevel)
	}

	if lvl, ok := zapLevelMap.Get(cfg.Level); ok {
		zapConfig.Level = zap.NewAtomicLevelAt(lvl)
	}

	zapConfig.EncoderConfig.TimeKey = "time"
	zapConfig.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder

	zapConfig.OutputPaths = []string{"stdout"}
	zapConfig.ErrorOutputPaths = []string{"stderr"}
	zapConfig.DisableStacktrace = true

	zapConfig.Encoding = cfg.Encoding

	zl, err := zapConfig.Build()
	if err != nil {
		return nil, err
	}
	l := zapr.NewLogger(zl)
	// return Logger{l, c.contextExtractors}
	return &l, nil
}

func SetGlobalLogger(ll logr.Logger) {
	l = ll
}

func GetLoggerWithContext(ctx context.Context) logr.Logger {
	return WithTracingContextValues(l, ctx)
}


func WithTracingContextValues(l logr.Logger, ctx context.Context) logr.Logger {
	span := trace.SpanFromContext(ctx)
	sc := span.SpanContext()
	ctxValues := make([]interface{}, 0)

	if sc.HasTraceID() {
		ctxValues = append(ctxValues, "traceId", sc.TraceID())
	}

	if sc.HasSpanID() {
		ctxValues = append(ctxValues, "spanId", sc.SpanID())
	}
	return l.WithValues(ctxValues...)
}
