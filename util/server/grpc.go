package server

import (
	"context"
	"fmt"
	"command/util/tracing/tracer"
	"log"

	grpc_ctx "command/util/server/grpc/ctx"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/validator"
	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"google.golang.org/grpc"
)

type grpcConfig struct {
	Addr                     Listen
	ServerUnaryInterceptors  []grpc.UnaryServerInterceptor
	ServerStreamInterceptors []grpc.StreamServerInterceptor
	ServerOption             []grpc.ServerOption
	MaxConcurrentStreams     uint32
}

func createDefaultGrpcConfig() *grpcConfig {
	grpc_prometheus.EnableHandlingTimeHistogram()
	config := &grpcConfig{
		Addr: Listen{
			Host: "0.0.0.0",
			Port: 10443,
		},
		ServerUnaryInterceptors: []grpc.UnaryServerInterceptor{
			NoCancelInterceptor(),
			tracer.UnaryServerInterceptorExcludedHealth(),
			grpc_prometheus.UnaryServerInterceptor,
			grpc_ctxtags.UnaryServerInterceptor(grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor)),
			grpc_ctx.UnaryServerInterceptor(),
			//grpc_logr.UnaryServerInterceptor(lgr),
			//grpc_zap.UnaryServerInterceptor(logger),
			grpc_validator.UnaryServerInterceptor(),
		},
		ServerStreamInterceptors: []grpc.StreamServerInterceptor{
			otelgrpc.StreamServerInterceptor(),
			grpc_prometheus.StreamServerInterceptor,
			grpc_ctxtags.StreamServerInterceptor(grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor)),
			grpc_ctx.StreamServerInterceptor(),
			//grpc_logr.StreamServerInterceptor(lgr),
			//grpc_zap.StreamServerInterceptor(logger),
			grpc_validator.StreamServerInterceptor(),
		},

		MaxConcurrentStreams: 1000,
	}

	return config
}

func (c *grpcConfig) ServerOptions() []grpc.ServerOption {
	return append(
		[]grpc.ServerOption{
			grpc_middleware.WithUnaryServerChain(c.ServerUnaryInterceptors...),
			grpc_middleware.WithStreamServerChain(c.ServerStreamInterceptors...),
			grpc.MaxConcurrentStreams(c.MaxConcurrentStreams),
		},
		c.ServerOption...,
	)
}

// grpcServer wraps grpc.Server setup process.
type grpcServer struct {
	server *grpc.Server
	config *grpcConfig
}

func newGrpcServer(c *grpcConfig, servers []ServiceServer) *grpcServer {
	s := grpc.NewServer(c.ServerOptions()...)
	for _, svr := range servers {
		svr.RegisterWithServer(s)
	}
	grpc_prometheus.Register(s)
	return &grpcServer{
		server: s,
		config: c,
	}
}

// Serve implements Server.Server
func (s *grpcServer) Serve() error {
	l, err := s.config.Addr.CreateListener()
	if err != nil {
		return fmt.Errorf("failed to create listener %w", err)
	}

	log.Println("gRPC server is starting ", l.Addr())

	err = s.server.Serve(l)

	if err != nil {
		log.Println(err)
		return fmt.Errorf("failed to serve gRPC server %w", err)
	}
	log.Println("gRPC server ready")

	return nil
}

// Shutdown
func (s *grpcServer) Shutdown(ctx context.Context) {
	s.server.GracefulStop()
}
