package server

import (
	"context"
	"encoding/json"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"net/http"
	"net/http/pprof"
	"strings"

	_ "github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	_ "go.opentelemetry.io/otel/exporters/prometheus"
)

type HTTPServerHandler func(*http.ServeMux)

func PrometheusHandler(httpMux *http.ServeMux) {
	httpMux.Handle("/metrics", promhttp.Handler())
}
func PprofHandler(httpMux *http.ServeMux) {
	// Register pprof handlers
	httpMux.HandleFunc("/debug/pprof/", pprof.Index)
	httpMux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	httpMux.HandleFunc("/debug/pprof/profile", pprof.Profile)
	httpMux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	httpMux.HandleFunc("/debug/pprof/trace", pprof.Trace)
}

type errorBody struct {
	Err  string `json:"error,omitempty"`
	Msg  string `json:"message,omitempty"`
	Code uint32 `json:"code,omitempty"`
}

func CustomHTTPError(ctx context.Context, mux *runtime.ServeMux, marshaler runtime.Marshaler, w http.ResponseWriter, r *http.Request, err error) {
	if strings.HasPrefix(r.URL.Path, "/health/ready") {
		runtime.DefaultHTTPErrorHandler(ctx, mux, marshaler, w, r, err)
		return
	}
	const fallback = `{"error": "failed to marshal error message"}`

	contentType := "application/json"
	if marshaler != nil {
		s := status.Convert(err)
		pb := s.Proto()
		contentType = marshaler.ContentType(pb)
	}
	w.Header().Set("Content-type", contentType)
	w.WriteHeader(http.StatusOK)
	st := status.Convert(err)
	jErr := json.NewEncoder(w).Encode(errorBody{
		Err:  st.String(),
		Msg:  st.Message(),
		Code: uint32(st.Code()),
	})

	if jErr != nil {
		_, _ = w.Write([]byte(fallback))
	}
}

func HttpMiddlewareCaptureHeader() HTTPServerMiddleware {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			md := make(metadata.MD)
			for k := range r.Header {
				k2 := strings.ToLower(k)
				md[k2] = []string{r.Header.Get(k)}
			}
			ctx := metadata.NewIncomingContext(r.Context(), md)
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		})
	}
}