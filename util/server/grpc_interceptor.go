package server

import (
	"context"

	"google.golang.org/grpc"
)

// "google.golang.org/grpc/codes"
// "google.golang.org/grpc/status"
// "google.golang.org/grpc/status"

// UnaryInterceptorAuthAttachUser used to handle the current user & roles by JWT/IAM servce.
// func UnaryInterceptorAuthAttachUser(prefix string, clientIAM iam.IClient, roles []string) grpc.UnaryServerInterceptor {
// 	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
// 		if !strings.HasPrefix(info.FullMethod, prefix) {
// 			return handler(ctx, req)
// 		}
// 		if clientIAM == nil {
// 			return nil, status.Errorf(codes.Internal, "Missing iam.IClient")
// 		}
// 		user, err := clientIAM.GetUser(ctx)
// 		if err != nil {
// 			return nil, status.Errorf(codes.Unauthenticated, err.Error())
// 		}
// 		// Verify user authorization
// 		if !user.HasAllRoles(roles...) {
// 			return nil, status.Errorf(codes.PermissionDenied, ForbiddenMesage)
// 		}
// 		newCtx := context.WithValue(ctx, Identity, user)
// 		return handler(newCtx, req)
// 	}
// }

// // UnaryInterceptorAuthAttachStandardClaims used to handle the current user & roles by JWT/IAM servce.
// func UnaryInterceptorAuthAttachStandardClaims(prefix string, clientIAM iam.IClient) grpc.UnaryServerInterceptor {
// 	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
// 		if !strings.HasPrefix(info.FullMethod, prefix) {
// 			return handler(ctx, req)
// 		}
// 		if clientIAM == nil {
// 			return nil, status.Errorf(codes.Internal, "Missing iam.IClient")
// 		}
// 		claims, err := clientIAM.GetStandardClaims(ctx)
// 		if err != nil {
// 			return nil, status.Errorf(codes.Unauthenticated, err.Error())
// 		}
// 		newCtx := context.WithValue(ctx, StandardClaims, claims)
// 		return handler(newCtx, req)
// 	}
// }

func NoCancelInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		return handler(withoutCancel(ctx), req)
	}
}

type noCancel struct {
	context.Context
}

func (c noCancel) Done() <-chan struct{} { return nil }
func (c noCancel) Err() error            { return nil }

// withoutCancel returns a context that is never canceled.
func withoutCancel(ctx context.Context) context.Context {
	return noCancel{Context: ctx}
}
