package grpc_logr

import (
	"context"
	"time"

	"command/util"
	"github.com/go-logr/logr"
)

func logFinalClientLine(ctx context.Context, logger logr.Logger, o *options, startTime time.Time, err error, msg string) {
	code := o.codeFunc(err)
	logr := util.WithTracingContextValues(logger, ctx).WithValues(
		"grpc.code", code.String(),
		"grpc.time_ms", durationToMilliseconds(time.Since(startTime)),
	)
	if err != nil {
		logr.Error(err, msg)
	} else {
		logr.Info(msg)
	}

}
