package main

import (
	"github.com/urfave/cli/v2"
	"os"
)

var Version = "0.1"

func main() {
	if err := run(os.Args); err != nil {
		panic(err)
	}
}

func run(_ []string) error {
	app := cli.NewApp()
	app.Name = "service"
	//app.Usage = ""
	app.Version = Version
	app.Commands = []*cli.Command{
		{
			Name:   "server",
			Usage:  "start grpc/http server",
			Action: serverAction,
		},
		{
			Name:   "example",
			Usage:  "patch data mambu custom fields",
			Action: exampleAction,
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name:    "first",
					Aliases: []string{"f"},
					Usage:   "first argument",
				},
				&cli.StringFlag{
					Name:        "second",
					Aliases:     []string{"s"},
					Usage:       "second argument",
					EnvVars:     nil,
					FilePath:    "",
					Required:    false,
					Hidden:      false,
					TakesFile:   false,
					Value:       "",
					DefaultText: "",
					Destination: nil,
					HasBeenSet:  false,
				},
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		panic(err)
	}
	return nil
}
