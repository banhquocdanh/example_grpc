package main

import (
	"command/config"
	"command/internal/service"
	"command/util"
	"command/util/server"
	"fmt"
	"github.com/go-logr/logr"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/urfave/cli/v2"
	"google.golang.org/protobuf/encoding/protojson"
)

var log *logr.Logger

func serverAction(_ *cli.Context) error {
	var err error
	var cfg *config.Config

	cfg, err = config.LoadConfig()
	if err != nil {
		return err
	}

	log, err = util.NewLogger(&cfg.Log)
	if err != nil {
		return err
	}

	log.Info("Start Service.....", "config", cfg)
	svc, err := service.NewService(cfg)
	if err != nil {
		return err
	}

	var customGatewayHandlers []server.HTTPServerHandler
	commandServer, err := server.New(
		server.WithGrpcAddrListen(cfg.Server.GRPC),
		server.WithGrpcServerUnaryInterceptors(grpc_middleware.ChainUnaryServer(
			grpc_prometheus.UnaryServerInterceptor,
		)),
		server.WithGatewayServerHandler(customGatewayHandlers...),
		server.WithServiceServer(svc),
		server.WithGatewayAddrListen(cfg.Server.HTTP),
		server.WithGatewayMuxOptions(runtime.WithErrorHandler(server.CustomHTTPError),
			runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{
				MarshalOptions: protojson.MarshalOptions{
					EmitUnpopulated: true,
					UseProtoNames:   true,
				},
				UnmarshalOptions: protojson.UnmarshalOptions{
					DiscardUnknown: true,
				},
			})),
		server.WithGatewayServerMiddlewares(server.HttpMiddlewareCaptureHeader()),
	)
	if err != nil {
		return nil
	}
	err = commandServer.Serve()
	if err != nil {
		log.Error(err, "service Serve")
		return err
	}

	log.Info("Service Stopped")
	return fmt.Errorf("implement me")
}
