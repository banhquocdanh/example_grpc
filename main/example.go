package main

import (
	"fmt"
	"github.com/urfave/cli/v2"
)

func exampleAction(command *cli.Context) error {
	firstArgument := command.String("first")
	secondArgument := command.String("second")
	fmt.Printf("Example agrs: %s | %s\n", firstArgument, secondArgument)
	return nil
}
